#! /bin/bash

#
# Author: Badr BADRI © pythops
#

set -e

# Check if board type is specified
if [ ! $JETSON_NANO_BOARD ]; then
	printf "\e[31mJetson nano board type must be specified\e[0m\n"
	exit 1
fi

ARCH=arm64
RELEASE=bionic
case "$JETSON_NANO_BOARD" in
    jetson-nano-2gb)
        # Jetson Nano, Nano 2GB and TX1
        RELEASE=bionic
        ;;

    jetson-nano)
        # Jetson Nano, Nano 2GB and TX1
        RELEASE=bionic
        ;;

    jetson-xavier-nx)
        # Jetson AGX Xavier Series, Xavier NX and TX2 Series
        RELEASE=focal
        ;;

    *)
	printf "\e[31mUnknown Jetson nano board L4T Release\e[0m\n"
	exit 1
        ;;
esac
printf "\e[32mPreparing rootfs based on $RELEASE\n"

# Check if the user is not root
if [ "x$(whoami)" != "xroot" ]; then
	printf "\e[31mThis script requires root privilege\e[0m\n"
	exit 1
fi

# Check for env variables
if [ ! $JETSON_ROOTFS_DIR ]; then
	printf "\e[31mYou need to set the env variable \$JETSON_ROOTFS_DIR\e[0m\n"
	exit 1
fi

# Install prerequisites packages
printf "\e[32mInstall the dependencies...  "
apt-get update > /dev/null
apt-get install --no-install-recommends -y qemu-user-static debootstrap binfmt-support coreutils parted wget gdisk e2fsprogs > /dev/null
printf "[OK]\n"

# Create rootfs directory
printf "Create rootfs directory...    "
mkdir -p $JETSON_ROOTFS_DIR
printf "[OK]\n"

# Run debootstrap first stage
printf "Run debootstrap first stage...  "
debootstrap \
        --arch=$ARCH \
        --foreign \
        --variant=minbase \
        --include=python3,python3-apt \
        $RELEASE \
	$JETSON_ROOTFS_DIR > /dev/null
printf "[OK]\n"

cat <<EOF > $JETSON_ROOTFS_DIR/etc/resolv.conf
nameserver 1.1.1.1
EOF

cp /usr/bin/qemu-aarch64-static $JETSON_ROOTFS_DIR/usr/bin

# Run debootstrap second stage
printf "Run debootstrap second stage... "
chroot $JETSON_ROOTFS_DIR /bin/bash -c "/debootstrap/debootstrap --second-stage" > /dev/null
printf "[OK]\n"

# Update /etc/apt/sources.list
printf "Update /etc/apt/sources.list"
#echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE main >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE restricted >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-updates main restricted >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE universe >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-updates universe >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE multiverse >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-updates multiverse >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-backports main restricted universe multiverse >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-security main restricted >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-security universe >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
echo deb http://ports.ubuntu.com/ubuntu-ports/ $RELEASE-security multiverse >> $JETSON_ROOTFS_DIR/etc/apt/sources.list
printf "[OK]\n"

printf "Creating /proc files to enable installing udisks2...        "
mkdir -p $JETSON_ROOTFS_DIR/proc/1
touch $JETSON_ROOTFS_DIR/proc/1/root
printf "[OK]\n"

printf "Success!\n"
