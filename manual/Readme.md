# Jetson images - manually generating an image

Create Ubuntu images for Nvidia Jetson boards with pre-installed SAT metalab software.

Fork from: https://github.com/pythops/jetson-nano-image

## Table of Contents:

- [Jetson images - manually generating an image](#jetson-images---manually-generating-an-image)
  - [Table of Contents:](#table-of-contents)
  - [Spec:](#spec)
  - [Supported boards:](#supported-boards)
  - [Define variables](#define-variables)
    - [JETSON\_NANO\_BOARD](#jetson_nano_board)
    - [JETSON\_NANO\_REVISION](#jetson_nano_revision)
    - [JETSON\_BUILD\_DIR](#jetson_build_dir)
    - [JETSON\_ROOTFS\_DIR](#jetson_rootfs_dir)
  - [Building the image](#building-the-image)
    - [Create root file system](#create-root-file-system)
    - [Customize image for Jetson](#customize-image-for-jetson)
    - [Install Metalab tools](#install-metalab-tools)
    - [Create the image](#create-the-image)
  - [Flash the generated image](#flash-the-generated-image)
  - [License](#license)

## Spec:

**Ubuntu release**: 18.04 (nano) or 20.04 (xavier)

**BSP**: 32.6.1 (nano) or 35.1.0 (xavier)

## Supported boards:
- [Jetson nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)
- [Jetson nano 2GB](https://developer.nvidia.com/embedded/jetson-nano-2gb-developer-kit)
- [Jetson Xavier NX](https://developer.nvidia.com/embedded/jetson-xavier-nx-devkit)

## Define variables

### JETSON_NANO_BOARD

You will need to specify the Jetson board you are using:

Jetson nano board:

```bash
export JETSON_NANO_BOARD=jetson-nano
```

Jetson Nano board 2GB:

```bash
export JETSON_NANO_BOARD=jetson-nano-2gb
```

Jetson Xavier NX board:

```bash
export JETSON_NANO_BOARD=jetson-xavier-nx
```

### JETSON_NANO_REVISION

For the Jetson Nano board 4GB only, you can specify which board model you wanna use `B01` or `A02` model. If you buy a new board now, chances are you're gonna get the `B01` model.

To specify your model, you need to define a new environment variable $JETSON_NANO_REVISION as follows:

For `B01` model (the default):

```bash
export JETSON_NANO_REVISION=300
```

For `A02` model:

```bash
export JETSON_NANO_REVISION=200
```

### JETSON_BUILD_DIR

We also need to define a build directory as well using the environment variable `$JETSON_BUILD_DIR`. This path will be created if it does not exist.

```bash
export JETSON_BUILD_DIR=/path/to/build_dir
```

### JETSON_ROOTFS_DIR

We also need to define the desired location of the rootfs:

```bash
export JETSON_ROOTFS_DIR=/path/to/rootfs
```

## Building the image

4 main steps:
- Create a root file system
- Customize image for Jetson
- Install Metalab tools
- Create the image and flash it on a SD card

### Create root file system

We build the rootfs by running the following script:

```bash
sudo -E ./create-rootfs.sh
# ROOTFS_DIR: ~/jetson-rootfs
# Installing the dependencies...  [OK]
# Creating rootfs directory...    [OK]
# Downloading the base image...   [OK]
# Run debootstrap first stage...  [OK]
# Run debootstrap second stage... [OK]
# Success!
```

### Customize image for Jetson

We first need to customize the image using `ansible`, this will install useful packages and create the user and password defined in the file [ansible/roles/jetson/defaults/main.yaml](./ansible/roles/jetson/defaults/main.yaml)

Install Ansible in the host machine:

```bash
pip install --user ansible
```

You can run the playbook as follows

```bash
pushd ansible
sudo -E $(which ansible-playbook) jetson.yaml
popd
```

Then we need to patch the rootfs with L4T by executing the following script:

```bash
sudo -E ./patch-rootfs.sh
# Build the image ...
# Extract L4T...       [OK]
# patching file /PATH/TO/jetson-images/build/Linux_for_Tegra/nv_tegra/nv-apply-debs.sh
# Apply L4T...        [OK]
# Update /etc/apt/sources.list.d/nvidia-l4t-apt-source.list...        [OK]
# Patch tegraflash_internal.py...        [OK]
# Rootfs patched successfully
```

### Install Metalab tools

You can run the playbook performing [these tasks](ansible/roles/sat-metalab/tasks/main.yml) as follows

```bash
pushd ansible
sudo -E $(which ansible-playbook) sat-metalab.yaml
popd
```

### Create the image

In order to flash the SD card to boot on the Jetson, we first need to pack everything on a single image.

To build the image:

```bash
sudo -E ./create-image.sh
# Image created successfully
# Image location: /path/to/jetson.img
```

## Flash the generated image

Make sure the `JETSON_BUILD_DIR` variable is set, or change the image path manually when executing the flash-image command.
If you followed either [Option 1](#option-1-create-mpu-script) or [Option 2](#option-2-manually-generate-the-base-image) right before flashing the image, the variable should be already set.

Otherwise, you can run:

```bash
export JETSON_BUILD_DIR=/path/to/build_dir
```

Then insert your sd card and run this command:

```bash
sudo ./flash-image.sh $JETSON_BUILD_DIR/Linux_for_Tegra/tools/jetson.img /dev/sda
# .
# .
# .
# Success !
# Your sdcard is ready !
```

## License

- Copyright Badr BADRI @pythops
- Jetson Xavier NX support and pre-installation of SAT Metalab tools by Christian Frisson
- MPU configuration and auto-script by Edu Meneses

MIT
