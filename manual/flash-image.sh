#! /bin/bash

#
# Author: Badr BADRI © pythops
#

set -e

# Check if the user is not root
if [ "x$(whoami)" != "xroot" ]; then
	printf "\e[31mThis script requires root privilege\e[0m\n"
	exit 1
fi

# Check the arguments
if [ "$#" -ne 2 ]; then
	echo "flash-image.sh </path/to/jetson.img> </path/to/sdcard>"
	echo "example: ./flash-image.sh /tmp/jetson.img /dev/mmcblk0"
	exit 1
fi

img=$1
dev=$2

# Check that $dev is a block device
if [ ! -b $dev ] || [ "$(lsblk | grep -w $(basename $dev) | awk '{print $6}')" != "disk" ]; then
	printf "\e[31m$dev is not a block device\e[0m\n"
	exit 1
fi

# Check jetson image file
if [ ! -e $img ] || [ ! -s $img ]; then
	printf "\e[31m$img does not exist or has 0 B in size\e[0m\n"
	exit 1
fi

# Unmount sdcard
if [ "$(mount | grep $dev)" ]; then
	printf "\e[32mUnmount SD card... "
	for mount_point in $(mount | grep $dev | awk '{ print $1}'); do
		sudo umount $mount_point > /dev/null
	done
	printf "[OK]\e[0m\n"
fi

# Flash image
printf "\e[32mFlash the sdcard... \e[0m"
dd if=$img of=$dev bs=4M conv=fsync status=progress
printf "\e[32m[OK]\e[0m\n"

# Extend the partition
printf "\e[32mExtend the partition... "
partprobe $dev &> /dev/null

sgdisk -e $dev > /dev/null

partprobe $dev &> /dev/null

# Recreate the partition
sgdisk -d 1 $dev > /dev/null

sgdisk -N 1 $dev > /dev/null

sgdisk -c 1:APP $dev > /dev/null

partprobe $dev &> /dev/null

printf "[OK]\e[0m\n"

# Extend fs
sleep 5
printf "\e[32mExtend the fs...\n"
e2fsck -fp $dev"p1" || e2fsck -y $dev"p1"
sleep 5
resize2fs $dev"p1"
sleep 5
sync
printf "[OK]\e[0m\n"

printf "\e[32mSuccess!\n"
printf "\e[32mYour sdcard is ready!\n"
