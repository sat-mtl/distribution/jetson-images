# Metalab - NVIDIA Jetson images

Create Ubuntu images for Nvidia Jetson boards with pre-installed SAT metalab software.

## Table of Contents:

- [Metalab - NVIDIA Jetson images](#metalab---nvidia-jetson-images)
  - [Table of Contents:](#table-of-contents)
  - [How to generate an image](#how-to-generate-an-image)
    - [Option 1: Generate the MPU using the script](#option-1-generate-the-mpu-using-the-script)
    - [Option 2: manually generate the base image](#option-2-manually-generate-the-base-image)
  - [Specs](#specs)
  - [User manual](#user-manual)
  - [License](#license)

## How to generate an image

### Option 1: Generate a Jetson Media Processing Unit (MPU) using the script

[Media Processing Units (MPU)](https://github.com/Puara/MPU) are a concept of the [Puara Framework](https://github.com/Puara).
They are created to deploy immersive environments, new media installations, and New Interfaces for Music Expression.
This option will generate a ready-to-use NVIDIA Jetson with the supported SAT/Metalab tools.

You can follow the instructions on the [MPU README page](./mpu/Readme.md) to generate the base MPU image in a couple of steps. 
It is necessary to use the [NVIDIA SDK Manager](https://developer.nvidia.com/drive/sdk-manager) and connect the Jetson board to a monitor, keyboard, and mouse to complete all the steps.

More information about the MPU recipe can be found [here](mpu/mpu_building_os.md).

### Option 2: manually generate the base image

To manually generate an Jetson image to use with SAT/Metalab tools, follow the instructions on the [manual process README page](./manual/Readme.md) to generate the base image.

## Specs

- OS version: The OS used to generate the images changes according to the Jetson version: Jetson Nano uses Ubuntu release 18.04, and Xavier uses Ubuntu 20.04.
- Supported boards:
  - [Jetson nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)
  - [Jetson nano 2GB](https://developer.nvidia.com/embedded/jetson-nano-2gb-developer-kit)
  - [Jetson Xavier NX](https://developer.nvidia.com/embedded/jetson-xavier-nx-devkit)

## User manual

Information on how to use the MPU can be found at the [MPU user manual](./mpu/mpu_user_manual.md). 
This document is a work in progress and does not currently cover all of the specifics and SAT/Metalab tools available for the system.

## License

- Jetson Xavier NX support and pre-installation of SAT Metalab tools by Christian Frisson
- MPU configuration by Edu Meneses
- Manual process: Copyright Badr BADRI @pythops

MIT
