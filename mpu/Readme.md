# MPU Building Recipe

- [MPU Building Recipe](#mpu-building-recipe)
  - [Creating the base image](#creating-the-base-image)
      - [Flash the base Jetpack OS](#flash-the-base-jetpack-os)
    - [First Config](#first-config)
      - [Set Jetson to never screen lock or turn the screen off](#set-jetson-to-never-screen-lock-or-turn-the-screen-off)
      - [Enable VNC (for Gnome only) and ssh](#enable-vnc-for-gnome-only-and-ssh)
  - [Configure the MPU](#configure-the-mpu)
  - [OPTIONAL:](#optional)
    - [Installing a Wi-Fi dongle driver](#installing-a-wi-fi-dongle-driver)
    - [Configure AP](#configure-ap)
    - [Set GNOME wallpaper](#set-gnome-wallpaper)


This file contains information on how to generate an MPU image for NVIDIA Jetson boards.

## Creating the base image

Use the following guide to download and flash the official Jetpack distribution. 
The script to generate the MPU can be executed from the NVIDIA Jetson after Jetpack is properly installed.

#### Flash the base Jetpack OS

- Download the NVIDIA SDK Manager from [https://developer.nvidia.com/drive/sdk-manager](https://developer.nvidia.com/drive/sdk-manager).
  - OBS: The NVIDIA Jetson might need a QSPI update. If yes, download an older Jetpack version (4.x) at [https://developer.nvidia.com/embedded/jetpack-archive](https://developer.nvidia.com/embedded/jetpack-archive) and follow the instructions at [https://developer.nvidia.com/embedded/jetpack-archive](https://developer.nvidia.com/embedded/jetpack-archive).
- Flash Jetpack into your NVIDIA Jetson. This step should be straightforward. However, if you plan to use [LivePose](https://gitlab.com/sat-mtl/tools/livepose) or install the MPU on an added NVMe disk, there are specific steps to follow:
  - Force NVIDIA Jetson recovery mode using a jumper to connect the **FS Rec** pin to **GND**. Please refer to the [Seed Studio Wiki](https://wiki.seeedstudio.com/install_NVIDIA_software_to_Jetson-202/#force-recovery-mode) if using one of their carrier boards. It is possible to force a recovery mode to fail a flash procedure. To do so, choose the proper flashing options and carefully follow these steps:
    - If Jetson status is **normal**, select _automatic_ setup, _Pre-config_ ...
    - ... Select to flash to _NVMe_ for the USB-C version. If flashing an older Jetson version (micro-USB) an extra flash to _eMMC_ is needed to update from Ubuntu 18.04 to Ubuntu 20.04 
    - Set the desired user and password and start the flashing process
    - When flashing to NVMe, the SDK Manager will try to install the components and it will fail with the "Default IP is not avaliable" error.
    - Skip the installation of the components and return to Step 1. The Jetson will now be in **recovery** mode
  - The easiest setup to flash Jetpack is to have both the host machine and the Jetson connected to a keyboard, mouse, and monitor
  - After entering Recovery Mode, connect the Jetson to the host computer through USB and open the NVIDIA SDK Manager 1.9.1.X. The host machine should recognize the connected Jetson.
  - Choose the proper board on the list and check the jetson status (on the tree dots icon)
  - Click to continue to step 2
  - Choose the desired components and agree with the Terms and Conditions
  - Proceed with a new flashing attempt choosing to flash the _eMMC_ and using _Manual Setup_. The installation will continue and complete successfully
  - The NVIDIA Jetson will reboot at the end of the process. If using a jumper to force recovery mode, do not forget to remove the jumper to reboot the NVIDIA Jetson.

- During Jetson's first boot: accept EULA and do basic config:
    - Set user: default **metalab**
    - Set hostname: default **satmeta-300X** (where X can be replaced by the Jetson's ID)
    - Set processor mode: use max available cores
    - Finish the wizard and wait for the reboot

### First Config

For the first config, it is necessary internet access (though ethernet), a keyboard, a mouse, and a video monitor. 

- Follow the first boot wizard:
  - Read and accept the license terms
  - Choose the language
  - Choose the appropriate keyboard layout
  - Select the proper time zone
  - Set the user, machine name, and password
    - Default user: **mpu**
    - Default computer's name: **MPUXXX** (usually MPU + id, replace it with the MPU's desired name/hostname/AP-SSID)
    - Default password: **mappings**
    - Select to log in automatically
  - Select the size to expand the partition (default: max size)
  - Optional: install Chromium Browser

After installation, the system will ask to reboot. Follow the Ubuntu welcome. 

#### Set Jetson to never screen lock or turn the screen off

- Enter Gnome settings
  - Power
    - Set blank screen to `Never`
  - Privacy
    - Set Automatic Screen Lock to `OFF`
    - Set Lock Screen on Suspend to `OFF`

#### Enable VNC (for Gnome only) and ssh

- Enter Gnome settings
  - Turn ON Sharing
    - Require password: `mappings`

- reboot

From this point, you can follow the next section remotely using SSH.

## Configure the MPU

- Ssh (with the X11-Forwarding flag) to the NVIDIA Jetson: `ssh -X mpu@<ip_address>` or `ssh -X mpu@MPUXXX.local` (replace MPUXXX with the chosen MPU's name). Obs: it is important to enable X11 forwarding as the installed SuperCollider version needs it to run sclang and install SATIE. For the first connection, it might be needed to exit and ssh again to properly create the _Xauthority_ file.

- Copy and paste the code block below to automatically run the [MPU Script](./mpu_building_os.md) routine:

```bash
wget "https://gitlab.com/sat-mtl/distribution/jetson-images/-/raw/master/mpu/scripts/mpu_jetson.sh" -O - | sh
```
 
## OPTIONAL: 

### Installing a Wi-Fi dongle driver

To create an access point we need a WiFi dongle (Jetson boards do not have built-in Wi-Fi). This is an example of how to install the driver for TP-Link AC600 Archer T2U Nano WiFi USB adapter.
For a different adapter, please check the manufacturer.

Plug the dongle and run the following commands

```bash
mkdir -p /sources && \
sudo apt install -y dkms git build-essential libelf-dev && \
cd /sources && \
git clone https://github.com/aircrack-ng/rtl8812au.git && \
cd /sources/rtl8812au && \
sudo make dkms_install
```

Unplug the TP-Link Archer T2U nano adapter and plug it again. The LED will start to blink. Verify if the driver is installed and loaded using the command `sudo dkms status`.

### Configure AP

```bash
sudo nmcli dev wifi hotspot ifname wlan0 ssid MPU010 password "mappings"
sudo sed -i -e 's/autoconnect=false/autoconnect=true/' /etc/NetworkManager/system-connections/Hotspot.nmconnection
```

### Set GNOME wallpaper

```bash
gsettings set org.gnome.desktop.background picture-uri 'file:///sources/MPU/mpu/wallpaper.png'
gsettings set org.gnome.desktop.background picture-options 'scaled'
```
