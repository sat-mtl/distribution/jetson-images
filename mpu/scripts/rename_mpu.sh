#!/bin/bash


# exit when any command fails
set -e

# keep track of the last executed command
trap 'last_command=$current_command; current_command=$BASH_COMMAND' DEBUG
# echo an error message before exiting
trap 'echo "\"${last_command}\" command filed with exit code $?."' EXIT

echo
echo "┌────────────────────┐"
echo "│ MPU Rename Script  |"
echo "│ Edu Meneses - 2022 |"
echo "│ Metalab - SAT      |"
echo "│ IDMIL - CIRMMT     |"
echo "└────────────────────┘"
echo
echo "Don't forget to provide the MPU's name"
echo "as an argument for this script"
echo

if [ -z "$1" ]
then
    echo "No argument was provided."
    echo "The script will be generate using MPUXXX as"
    echo "the MPU's name."
    echo
    MPUid=MPUXXX
else
    MPUid=$1
    echo "MPU's new name: ${MPUid}"
    echo
fi

read -r -s -p $'Press enter to continue...\n\n'

sudo sed -i -e "s/^        format = \"%y-%m-%d %H:%M.*$/        format = \"%y-%m-%d %H:%M (${MPUid})\"/" /etc/i3status.conf

sudo sed -i -e "s/        server string = .*$/        server string = ${MPUid}/" /etc/samba/smb.conf

sudo sed -i -e "s/^127.0.1.1.*$/127.0.1.1       ${MPUid}/" /etc/hosts

echo ${MPUid} > /etc/hostname

# If AP is active, must rename /etc/NetworkManager/system-connections/Hotspot.nmconnection as well

echo "Done renaming the MPU."
echo
echo "Please reboot the system for all changes to take effect."

trap - EXIT
